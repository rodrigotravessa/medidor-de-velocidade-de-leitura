#coding: utf-8
from tkinter import *
from tkinter.filedialog import askopenfilename
from tkinter import ttk
import time

global start_reading 
global word_count
word_count = 0

def escolher_texto():     
    global start_reading
    global word_count

    ABC = open(askopenfilename(), 'r', encoding='utf-8')
    texto_escolhido = ABC.read()
    word_count = len(texto_escolhido.split())
    texto_tela = Text(root,wrap=WORD, border=3, height=80, width=80)
    texto_tela.insert(INSERT, texto_escolhido)
    texto_tela.grid(column=1, row=5)
    tempo_init = Label(root, text='Começou a ler em {}'.format(time.strftime("%H:%M:%S")))
    tempo_init.grid(column=2, row=1)
    start_reading = time.time()

def finish_time():
    tempo_final = Label(root, text='terminou de ler em {}'.format(time.strftime("%H:%M:%S")))
    tempo_final.grid(column=2, row=3)
    finish_reading = time.time()
    tempo_gasto = finish_reading - start_reading
    ppm = Label(root, text='você levou {:.2f} segundos para ler,\n você leu {:.2f} palavras por minuto'.format(tempo_gasto, (word_count/tempo_gasto)*60))
    ppm.grid(column=2, row=4)

window_x = 900
window_y = 600


root = Tk()
root.tk.call('encoding', 'system', 'utf-8')
root.title = "Teste de Velocidade de Leitura"

root.geometry('{}x{}'.format(window_x, window_y))

choose_text = Button(root, text='escolha o texto', command=escolher_texto)
choose_text.grid(column=1, row=1, padx=300, pady=10)

terminado = Button(root, text='terminei de ler', command=finish_time)
terminado.grid(column=1, row=2)

root.mainloop()